from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^food_menu/', views.food_menu, name='food_menu'),
    url(r'^product_list/', views.product_list, name='product_list'),
    url(r'^choose_product/', views.choose_product, name='choose_product'),

    url(r'^set_user_name/', views.set_user_name, name='set_user_name'),
    url(r'^set_guest_name/', views.set_guest_name, name='set_guest_name'),
    url(r'^add_to_basket/', views.add_to_basket, name='add_to_basket'),
    url(r'^basket/', views.basket, name='basket'),
    url(r'^confirmed/', views.confirmed, name='basket'),
    url(r'^skip_waiting/', views.skip_waiting, name='skip_waiting'),
    url(r'^summary/', views.summary, name='summary'),
    url(r'^add_more/', views.add_more, name='add_more'),
    url(r'^remove/', views.remove, name='remove'),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^rate/', views.rate, name='rate'),
]