from django.contrib import admin

from django.contrib import admin
from order.models import *


class ProductAdmin(admin.ModelAdmin):
    pass
admin.site.register(Product, ProductAdmin)


class ProductGroupAdmin(admin.ModelAdmin):
    pass
admin.site.register(ProductGroup, ProductGroupAdmin)


class OrderAdmin(admin.ModelAdmin):
    pass
admin.site.register(Order, OrderAdmin)


class ProductSizeAdmin(admin.ModelAdmin):
    pass
admin.site.register(ProductSize, ProductSizeAdmin)