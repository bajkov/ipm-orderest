# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-11-24 01:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='order.ProductGroup'),
        ),
        migrations.AddField(
            model_name='productsize',
            name='product',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='order.Product'),
        ),
    ]
