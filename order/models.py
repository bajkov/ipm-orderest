from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class ProductGroup(models.Model):
    name = models.CharField(max_length=100)
    image = models.CharField(max_length=100, default='default', null=True)

    def get_image_path(self):
        return "/static/order/img/menu/" + self.image + ".png"

    def __str__(self):
        return str(self.name)
    pass


class Product(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    ingredients = models.TextField()
    image = models.CharField(max_length=100, default="default", null=True)
    product_group = models.ForeignKey(ProductGroup, on_delete=models.CASCADE, null=True)

    def get_image_path(self):
        return "/static/order/img/menu/" + self.image + ".png"

    def __str__(self):
        return str(self.name)
    pass


class ProductSize(models.Model):
    list_display = ('name', 'price')
    list_display_links = ('name', 'price')
    name = models.CharField(max_length=10)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.product.name + " " + self.name)
    pass


class SizeQuantity(models.Model):
    product_size = models.ForeignKey(ProductSize, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0, null=True)
    pass


class Order(models.Model):
    customers = models.TextField()
    products = models.ManyToManyField(SizeQuantity)
    status = models.CharField(max_length=10)
    table_number = models.IntegerField(null=True)
    payment_type = models.CharField(max_length=10, default=None, null=True, blank=True)

    def __str__(self):
        return '%s: %s' % (str(self.table_number), self.customers)
    pass


class Message(models.Model):
    rating = models.IntegerField(null=True, blank=True)
    text = models.TextField(null=True, blank=True)


