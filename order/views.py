import datetime

from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from .models import ProductGroup, Product, ProductSize, Order, SizeQuantity
from django.forms.models import model_to_dict


def index(request):
    template = loader.get_template('order/index.html')
    return HttpResponse(template.render(request))


@csrf_exempt
def food_menu(request):
    product_groups = ProductGroup.objects.all()
    return render(request, 'order/food_menu.html', {'product_groups': product_groups})


@csrf_exempt
def product_list(request):
    group_id = request.POST['group_id']
    group_name = ProductGroup.objects.get(pk=group_id).name
    products_list = Product.objects.filter(product_group_id=group_id)
    return render(request, 'order/product_list.html', {'products_list': products_list, 'group_name': group_name})


@csrf_exempt
def choose_product(request):
    product_id = request.POST['product_id']
    product = Product.objects.get(pk=product_id)
    sizes = ProductSize.objects.filter(product_id=product_id)
    return render(request, 'order/choose_product.html', {'product': product, 'sizes': sizes})


@csrf_exempt
def set_user_name(request):
    name = request.POST['user_name']
    request.session['user_name'] = name
    return HttpResponse('OK')


@csrf_exempt
def set_guest_name(request):
    name = request.POST['user_name']
    request.session['user_name'] = name
    return food_menu(request)


@csrf_exempt
def skip_waiting(request):
    request.session['delivered'] = True
    price = request.session.get('order_sum', None)
    name = request.session.get('user_name', None)
    return render(request, 'order/pay_options.html', {'name': name, 'price': price})


@csrf_exempt
def summary(request):
    request.session['order'] = []
    request.session['order_sum'] = 0
    request.session['delivered'] = False
    request.session['confirmed'] = False
    request.session['start_process_time'] = None
    name = request.session.get('user_name', None)
    return render(request, 'order/summary.html', {'name': name})


@csrf_exempt
def add_to_basket(request):
    size_id = request.POST.get('size_id', None)
    quantity = request.POST.get('quantity', None)

    if request.session.get('order', None) is None:
        request.session['order'] = []

    order = request.session.get('order')
    order_product = SizeQuantity()
    size = ProductSize.objects.get(pk=size_id)
    order_product.product_size = size
    order_product.quantity = quantity
    order_product.save()

    order.append(model_to_dict(order_product))
    request.session['order'] = order

    product_groups = ProductGroup.objects.all()
    return render(request, 'order/food_menu.html', {'product_groups': product_groups})


def basket(request):
    ordered_products = []
    order_sum = 0
    order = request.session.get('order')
    if request.session.get('confirmed', None) is None or request.session.get('confirmed', None) is False:
        if order is not None:
            for order_product in order:
                size = ProductSize.objects.get(pk=order_product["product_size"])
                price = float(size.price) * float(order_product["quantity"])
                order_sum += price
                order_product = {
                    'product_name': size.product.name,
                    'size': size.name,
                    'price_single': size.price,
                    'price_sum': price,
                    'quantity': order_product["quantity"],
                }
                ordered_products.append(order_product)
        request.session['order_sum'] = order_sum
        return render(request, 'order/basket.html', {'ordered_products': ordered_products, 'order_sum': order_sum})
    elif request.session.get('delivered', None) is None or request.session.get('delivered', None) is False:
        return confirmed(request)
    else:
        return skip_waiting(request)


def confirmed(request):
    request.session['confirmed'] = True
    if request.session.get('start_process_time', None) is None:
        request.session['start_process_time'] = datetime.datetime.now().strftime("%H:%M")

    return render(request, 'order/confirmed.html', {'time': request.session.get('start_process_time', None)})


@csrf_exempt
def add_more(request):
    request.session['confirmed'] = False
    request.session['start_process_time'] = None
    return food_menu(request)


@csrf_exempt
def remove(request):
    remove_item_id = request.POST.get('remove_item_id', None)
    id = int(remove_item_id) - 1
    order = request.session.get('order')
    order = list(order)
    order.pop(id)
    request.session['order'] = order
    return basket(request)


@csrf_exempt
def logout(request):
    request.session['order'] = []
    request.session['order_sum'] = 0
    request.session['delivered'] = False
    request.session['confirmed'] = False
    request.session['start_process_time'] = None
    request.session['user_name'] = None
    return index(request)


@csrf_exempt
def rate(request):
    return index(request)
